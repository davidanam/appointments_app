import { Component, ViewChild, ElementRef  } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AppointmentsProvider  } from "../../providers/appointments/appointments";
import { DepartmentsProvider } from "../../providers/departments/departments";


@IonicPage()
@Component({
  selector: 'page-app-edit',
  templateUrl: 'app-edit.html',
})
export class AppEditPage {
  appointment:any;
  appoint_body: string;
  department: string;
  departments: any = [];
  @ViewChild('myInput') myInput: ElementRef;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public appPro: AppointmentsProvider,
    public departPro: DepartmentsProvider,
    ) {
    this.appointment=this.navParams.get("appointment");
    this.appoint_body= this.appointment.description;
    this.department = this.appointment.department.department_id;
  }

  ionViewWillEnter(){
    this.get_departments();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppEditPage');
  }
  
  fecund_save(department){
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();

    let new_appointment = {
      department_id: this.department,
      body: this.appoint_body, 
    };
    console.log(new_appointment)

    if (this.appoint_body != "" ){
      this.appPro.edit_appointments(new_appointment, this.appointment.appointment_id).subscribe(data => {
        loader.dismiss();
        this.navCtrl.push("ProfilePage");
      }, error => {
        loader.dismiss();
      });
    }else{
      loader.dismiss();
    }
    

  }

  get_departments(){
    console.log(this.appointment);
    this.departPro.get_all_departments(this.appointment.organisation.organisation_id).subscribe( data => {
      this.departments = data.data;
    });
  }

  resize() {
    this.myInput.nativeElement.style.height = this.myInput.nativeElement.scrollHeight + 'px';
  }

}
