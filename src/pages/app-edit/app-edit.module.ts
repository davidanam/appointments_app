import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppEditPage } from './app-edit';

@NgModule({
  declarations: [
    AppEditPage,
  ],
  imports: [
    IonicPageModule.forChild(AppEditPage),
  ],
})
export class AppEditPageModule {}
