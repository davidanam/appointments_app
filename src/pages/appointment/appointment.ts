import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AppointmentsProvider} from '../../providers/appointments/appointments'
import { Storage} from '@ionic/storage';
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";


@IonicPage()
@Component({
  selector: 'page-appointment',
  templateUrl: 'appointment.html',
})
export class AppointmentPage {
  appointment_info: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public appointment: AppointmentsProvider, 
    public storage: Storage, 
    public alertCtrl: AlertController, 
    public formBuilder: FormBuilder) {
    this.appointment_info = this.navParams.get("appointment");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentPage');
  }

  cancelappointment(appointment){
    console.log(appointment);
    this.showConfirm(appointment);
  }

  editappointment(appointment){
   this.navCtrl.push("AppEditPage", {appointment:appointment}); 
  }

  showAlert(msg) {
    let alert = this.alertCtrl.create({
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  go_back(){
    this.navCtrl.setRoot("HomePage");
  }


  showConfirm(appointment) {
    let confirm = this.alertCtrl.create({
      message: 'Are sure you want to cancel this appoinment ?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.appointment.cancel_appointments(appointment).subscribe(data => { 
                if(data.flag){
                    this.navCtrl.pop();
                }
            })
          }
        }
      ]
    });
    confirm.present();
  }
  
}
