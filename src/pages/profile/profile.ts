import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import { UserProvider} from '../../providers/user/user';
import { AppointmentsProvider} from '../../providers/appointments/appointments';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  pet="appointments";
  appointments=[];
  myForm:any;
  user: any;

  
  constructor(public navCtrl: NavController, 
        public navParams: NavParams, 
        public userPro:UserProvider,
        public formBuilder: FormBuilder, 
        public appprovider: AppointmentsProvider,
        public storage: Storage,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
    ) {
    
  }

  ionViewWillEnter(){
     this.storage.get("puser").then( data => {
        this.user = data;
        this.myForm = this.formBuilder.group({
          location: [data.user_location, Validators.compose([Validators.pattern("^[a-zA-Z0-9_.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9]+$"), Validators.required])],
          phonenumber: [data.user_tel_no, Validators.compose([Validators.required, Validators.minLength(9)])],
          email: [data.user_email, Validators.compose([Validators.required, Validators.minLength(5)])],
          name: [data.user_name, Validators.compose([Validators.required, Validators.minLength(5)])]
        });
        this.getappointments(data.user_id);
     });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  getappointments(user_id){
    this.appprovider.get_appointments( user_id ).subscribe(data => {
      this.appointments = data.data;
    });
  }

  editprofile(){
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    let user  = this.myForm.value;
    this.userPro.editProfile(this.user.user_id, user).subscribe( res => {
      loader.dismiss();
      if( res.flag ){
        console.log(res);
        this.storage.set("puser", res.user);
        localStorage.setItem("tuser", res.user);
        this.user=res.user;
      }else{
        this.showAlert("Sorry , please try again later");
      }
    }, error =>{
      loader.dismiss();
      this.showAlert("Check your internet connection");      
    });
  }

  showAlert(msg) {
    let alert = this.alertCtrl.create({
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  move_to_appoimtment( appointment ){
    this.navCtrl.push("AppointmentPage", { appointment: appointment });
  }

  logout(){
    this.storage.set("puser", null);
    localStorage.setItem("tuser", null);
    this.navCtrl.setRoot("LoginPage");
  }

}
