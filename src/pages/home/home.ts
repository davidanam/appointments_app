import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { DepartmentsProvider } from "../../providers/departments/departments";
import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @ViewChild(Slides) slides:Slides;

  SkipMsg:String;
  departments: any = [];

  slidesOptions:any;
  constructor(
    public navCtrl: NavController, 
    public departsPro: DepartmentsProvider,
    private callNumber: CallNumber, 
    public navParams: NavParams) {
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad HomePage');
    this.get_all_departments();
  }

  get_all_departments(){
    this.departsPro.get_all_departments(6).subscribe( data => {
      this.departments = data.data;
    });
  }


  setappointment(department){
    this.navCtrl.push("DepartmentsPage", { department: department });
  }
  profile(){
    this.navCtrl.push("ProfilePage");
  }
 
  dail_number(){
    this.callNumber.callNumber("256753275650", true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }


}
