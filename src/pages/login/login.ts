import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import { UserProvider } from "../../providers/user/user";
import { Storage } from '@ionic/storage';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  

  myForm: any;
  constructor(
    public navCtrl: NavController, 
    public formBuilder: FormBuilder,
    public userPro: UserProvider,
    public storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {

    this.myForm = this.formBuilder.group({
      email  : ['', Validators.compose([Validators.pattern("^[a-zA-Z0-9_.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9]+$"), Validators.required])],
      password  : ['', Validators.compose([Validators.minLength(5), Validators.required])]
    });

  }
  login(){
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    // this.navCtrl.push("HomePage");
    let user = this.myForm.value;
    this.userPro.login(user).subscribe(res => {
      loader.dismiss();
      if (res.flag) {
       this.storage.set("puser", res.user);
        localStorage.setItem("tuser", res.user);
       this.navCtrl.push("HomePage");
      } else {
   this.showAlert("Password and email combination is wrong");
      }
    }, error => {
      loader.dismiss();
    this.showAlert("Check your internet connection");
   });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  signup(){
    this.navCtrl.push("SignupPage");
  }

  forgotPassWord(){
    this.navCtrl.push("ForgotpassPage");
  }

  
  showAlert(message){
    let alert = this.alertCtrl.create({
      
      subTitle: message,
      buttons: ['Dismiss']
    });
    alert.present();
  }
}
