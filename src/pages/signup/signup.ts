import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import { UserProvider} from '../../providers/user/user';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  mysignupForm:any;
  constructor(public navCtrl: NavController,
     public navParams: NavParams, public formBuilder: FormBuilder,
      public userPro: UserProvider,
     public storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
    ) {
    this.mysignupForm = this.formBuilder.group({
        email: ['', Validators.compose([Validators.pattern("^[a-zA-Z0-9_.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9]+$"), Validators.required])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(9)])],
        name: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
        phonenumber: ['', Validators.compose([Validators.required, Validators.minLength(5)])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }
  create_account(){
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    let user  = this.mysignupForm.value;
    console.log(user);
    this.userPro.create_a_user(user).subscribe( res => {
      loader.dismiss();
      if( res.flag ){
        console.log(res);
        this.storage.set("puser", res.user);
        localStorage.setItem("tuser", res.user);
        this.navCtrl.push("LoginPage");
      }else{
        this.showAlert("Sorry , please try again later");
      }
    }, error =>{
      loader.dismiss();
      this.showAlert("Check your internet connection");      
    });
  }
  showAlert(msg) {
    let alert = this.alertCtrl.create({
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
  login(){
    this.navCtrl.push("LoginPage");
  }
  }


