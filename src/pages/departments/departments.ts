import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AppointmentsProvider  } from "../../providers/appointments/appointments";

@IonicPage()
@Component({
  selector: 'page-departments',
  templateUrl: 'departments.html',
})
export class DepartmentsPage {
  department: any;
  user: any;
  appoint_body="";
  error: boolean = false;
  @ViewChild('myInput') myInput: ElementRef;
  constructor(
    public navCtrl: NavController, 
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public appPro: AppointmentsProvider,
    public navParams: NavParams) {
    this.department = this.navParams.get("department");
  }

  ionViewWillEnter(){
    this.storage.get("puser").then( data => {
      this.user = data;
      console.log(this.user);
    });
  }

  account(){
    this.navCtrl.push("ProfilePage");
  }

  make_appoiment(department){
    if (this.appoint_body != "" ){
      let loader = this.loadingCtrl.create({
        content: "Please wait...",
      });
      loader.present();

      let appointment = {
        user_id: this.user.user_id,
        department_id: this.department.department_id,
        organisation_id: 6,
        body: this.appoint_body,
      };
      this.appPro.make_appointments(appointment).subscribe(data => {
        loader.dismiss();
        this.navCtrl.setRoot("AppointmentPage", { appointment: data.res});
      }, error => {
        loader.dismiss();
      });
    }else{
      this.error = true;
    }
  }

  resize() {
    this.myInput.nativeElement.style.height = this.myInput.nativeElement.scrollHeight + 'px';
  }
  
}
