import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';



@IonicPage()
@Component({
  selector: 'page-forgotpass',
  templateUrl: 'forgotpass.html',
})
export class ForgotpassPage {
  email: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public user: UserProvider,
     public load: LoadingController, public alert: AlertController) {
  }

 forgotPass(){
   if(this.email.length>0){
    let loader = this.load.create({
      content: "Please wait..."  
    });
    loader.present();
    this.user.forgot_pass(this.email).subscribe(data=> {
      console.log(data);
     loader.dismiss();
     this.showAlert(data.msg);
     this.email=" ";
       })
   }

 }
 showAlert(text){
   this.alert.create({message:text,  buttons: ['OK']
   }).present();
 }

}
