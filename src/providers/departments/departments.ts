import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";

@Injectable()
export class DepartmentsProvider {
headers:Headers;
api_url: string;
options: RequestOptions;

  constructor(public http: Http) {
    this.api_url = "http://127.0.0.1/appointments_dashboard/welcome/";
   this.headers=new Headers();
   this.headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
   this.headers.append('Accept', 'application/json');
   this.options = new RequestOptions({ headers: this.headers });
  }

  get_all_departments(org_id){
    return this.http.get(this.api_url+'get_departments_for_app/' + org_id).map(res => res.json()); 
  }

}
