import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";



@Injectable()
export class AppointmentsProvider {
headers:Headers;
option:RequestOptions;
  url: string ="http://127.0.0.1/appointments_dashboard/welcome/";
  constructor(public http: Http) {
   this.headers=new Headers();
   this.headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
   this.headers.append('Accept', 'application/json');
   this.option = new RequestOptions({ headers: this.headers });
  }

make_appointments(data){
  let body = "user_id=" + data.user_id +
              "&department_id=" + data.department_id + 
              "&description=" + data.body + 
              "&organisation_id=" + data.organisation_id;
  return this.http.post(this.url +"make_appointment",encodeURI(body), this.option ).map(res=>res.json());

}

cancel_appointments(data){
  return this.http.get(this.url + "cancelappointment/"+data.appointment_id).map(res=>res.json());
}

edit_appointments(data, appointmentId){
  let body= "department_id=" + data.department_id + "&description=" + data.body;
  return this.http.post(this.url+"edit_appointment/"+appointmentId, encodeURI(body), this.option).map(res=>res.json());
}


get_appointments(user_id){
  return this.http.get(this.url + "get_appointments_for_user/" + user_id).map(res => res.json());
}

}
