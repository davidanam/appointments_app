// import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  api_url: string;
  headers: Headers;
  options: RequestOptions;
  constructor(public http: Http) {
    this.api_url = "http://127.0.0.1/appointments_dashboard/welcome/";
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
    console.log('Hello UserProvider Provider');
  }
  create_a_user(data) {
    console.log(data)
    let body = "username=" + data.name + "&email=" + data.email + "&phonenumber=" + data.phonenumber + "&password=" + data.password + "&address=" + data.address;
    return this.http.post(this.api_url + '/create_account', encodeURI(body), this.options).map(res => res.json());
  }
 
  login(data){
    let body = "email="+data.email+"&password="+data.password;
    return this.http.post(this.api_url + '/login_user_app', encodeURI(body), this.options).map(res => res.json());    
  }
  
  forgot_pass(email){
    let body = "email=" + email  
    return this.http.post(this.api_url + '/appforgotpassword', encodeURI(body), this.options).map(res => res.json());
  }
  editProfile(user_id,data){
    let body = "name=" + data.name + "&email=" + data.email + "&phonenumber=" + data.phonenumber + "&location=" + data.location ;
    return this.http.post(this.api_url + '/edit_account/'+user_id, encodeURI(body), this.options).map(res => res.json());   
  }
}
